'use strict'

var express     =   require('express');
var app         =   express();

const dataSiswa =   [
    {   
        id:1,
        nama:'Bagus',
        f1:{
            datalv:[
                80,90,70,70,100,80
            ]
        },
        f2:{
            datalv:[
                80,90,70,70,100,80
            ]
        },
        f3:{
            datalv:[
                20,2,30,2,70,50
            ]
        },
        f4:{
            datalv:[
                0,1,0,0,2,1
            ]
        },
    },
    {
        id:2,
        nama:'Ridho',
        f1:{
            datalv:[
                100,80,80,80,70,70
            ]
        },
        f2:{
            datalv:[
                100,80,80,80,70,70
            ]
        },
        f3:{
            datalv:[
                5,10,20,30,50,40
            ]
        },
        f4:{
            datalv:[
                2,1,1,1,0,1
            ]
        },
    },
    {
        id:3,
        nama:'Gustam',
        f1:{
            datalv:[
                90,90,90,70,80,70
            ]
        },
        f2:{
            datalv:[
                90,90,90,70,80,70
            ]
        },
        f3:{
            datalv:[
                5,3,30,50,70,90
            ]
        },
        f4:{
            datalv:[
                2,1,3,0,2,2
            ]
        },
    }
]

const convert   =   (x)=>{
    // console.log(x)
    switch(true) {
        case x >= 561 && x<= 600:
          // code block
        //   console.log(5)
          return 5
          break;
        case x >= 501 && x<= 560:
          // code block
        //   console.log(4)
          return 4
          break;
        case x >= 421 && x<= 500:
          // code block
        //   console.log(3)
          return 3
          break;
        case x >= 301 && x<= 420:
          // code block
        //   console.log(2)
          return 2
          break;
        case x >= 201 && x<= 300:
          // code block
        //   console.log(1)
          return 1
          break;
        case x >= 0 && x<= 200:
          // code block
        //   console.log(0)
          return 0
          break;
        default:
          // code block
      }
}

const convertlv   =   (x)=>{
    // console.log(x)
    switch(true) {
        case x >= 100:
          // code block
        //   console.log(4)
          return 4
          break;
        case x >= 90 && x<= 99:
          // code block
        //   console.log(3)
          return 3
          break;
        case x >= 80 && x<= 89:
          // code block
        //   console.log(2)
          return 2
          break;
        case x >= 70 && x<= 79:
          // code block
        //   console.log(1)
          return 1
          break;
        case  x< 70:
          // code block
        //   console.log(0)
          return 0
          break;
        default:
          // code block
      }
}

const convertTime   =   (x)=>{
    // console.log(x)
    switch(true) {
        case x >= 481 && x<= 600:
          // code block
        //   console.log(4)
          return 5
          break;
        case x >= 361 && x<= 480:
          // code block
        //   console.log(3)
          return 4
          break;
        case x >= 241 && x<= 360:
          // code block
        //   console.log(2)
          return 3
          break;
        case x >= 121 && x<= 240:
          // code block
        //   console.log(1)
          return 2
          break;
        case x >= 0 && x<= 120:
          // code block
        //   console.log(0)
          return 1
          break;
        default:
          // code block
      }
}
const convUlang   =   (x)=>{
    // console.log(x)
    switch(true) {
        case x <=0:
            return 5    
        case x >= 1 && x<= 2:
          // code block
        //   console.log(4)
            return 4
          break;
        case x >= 3 && x<= 4:
          // code block
        //   console.log(3)
            return 3
          break;
        case x >= 5 && x<= 6:
          // code block
        //   console.log(2)
            return 2
          break;
        case x >= 7 && x<= 8:
          // code block
        //   console.log(1)
            return 1
          break;
        case x >=9:
          // code block
        //   console.log(0)
            return 0
          break;
        default:
          // code block
      }
}

const getN3 =   (i)=>{
    const n = n2.map(l=>{
        if(l.id==i.id){
            return 0
        }else{
            const j =  [
                i.f1-l.f1<=0?0:1,
                i.f2-l.f2<=0?0:1,
                i.f3-l.f3<=0?0:1,
                i.f4-l.f4<=0?0:1,
            ]
            return (j.reduce(function(acc, val) { return acc + val; }, 0))/4
        }
    })
    // console.log(n)
    return n
}

const getEntring    =   (x)=>{
    const n = []
    n3.map(i=>{
        n.push(i.resTemp[x])
    })
    // console.log(n)
    return ((n.reduce(function(acc, val) { return acc + val; }, 0))/(dataSiswa.length-1)).toFixed(4)
}

const n1    =   dataSiswa.map(i=>{
    const j = {
        ...i,
        f2:{
            ...i.f2,
            convres:i.f2.datalv.map(p=>{
                // console.log(convertlv(p),p)
                return convertlv(p)
            })
        }
    }
    return j
})

const n2 =  n1.map(i=>{
    const k ={
        ...i,
        f1:convert(i.f1.datalv.reduce(function(acc, val) { return acc + val; }, 0)),
        f2:i.f2.convres.reduce(function(acc, val) { return acc + val; }, 0),
        f3:convertTime(i.f3.datalv.reduce(function(acc, val) { return acc + val; }, 0)),
        f4:convUlang(i.f4.datalv.reduce(function(acc, val) { return acc + val; }, 0))
    }
    return k
})

const n3 = n2.map(i=>{
    const h =   {
        id:i.id,
        nama:i.nama,
        resTemp:getN3(i)
    }
    return h
})

const n4 =  n3.map((i,idx)=>{
    const k = {
        ...i,
        leavingFlow:Number(((i.resTemp.reduce(function(acc, val) { return acc + val; }, 0))/(dataSiswa.length-1)).toFixed(4)),
        entringFlow:Number(getEntring(idx)),
        netFlow:Number(((i.resTemp.reduce(function(acc, val) { return acc + val; }, 0))/(dataSiswa.length-1)).toFixed(4)) - Number(getEntring(idx)),
    }
    return k
})

const finalRank =   n4.sort((a,b)=> b.netFlow - a.netFlow)
// console.log(n1[0])
// console.log(n2)
console.log(n3)
// console.log(n4)
console.log(finalRank)
console.log(`\n Peringkat Pertama Adalah ${finalRank[0].nama}`)

// convert(455)

// console.log(dataSiswa)